After installation by running installmod in a mod's directory it will 
copy the directory to it's proper place in openmw.cfg and enable it at 
the bottom of your mod list.

### Installation

Super easy. Clone this repository and run install.sh

### How to use

This program must be run in the folder with a .esp file OR meshes and 
textures folders. Run installmod and then it's installed!

Please!!! Don't use if there are more than 1 esp files. That currently breaks the program.