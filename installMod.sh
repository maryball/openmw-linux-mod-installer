#!/bin/sh
#TODO support more than 1 esp or ESM file in a folder

echo "NOTE THIS ONLY WORKS IF THERE IS 1 ESP FILE." 
echo "IF THERE ARE ANY MORE DO NOT USE THIS AND INSTALL MANUALLY"
echo "TO CONTINUE TYPE y"

read continue

if [ "$continue" != "y" ]; then
	exit 1
fi


dataInsert='data='
contentInsert='content='
quotes='"'
finalInsertDIR=$dataInsert$quotes$PWD$quotes #Makes the insert be data="dir" there is almost certainly an easier way to get this done but I just don't know it

configFile="/home/$USER/.config/openmw/openmw.cfg" 


#Installs ESM -> ESP -> OMWADDON
count=`ls -1 *.esm 2>/dev/null | wc -l` #This installs any esm files
if [ $count != 0 ]
then
        espFile=$(find . -name "*.esm") #catch .esp file
        espFile=${espFile##*/} #Annoying little thing with the find command is it adds a ./
        finalInsertESP=$contentInsert$espFile
        echo $finalInsertESP >> $configFile
fi

count=`ls -1 *.ESM 2>/dev/null | wc -l` #This installs any esm files
if [ $count != 0 ]
then
        espFile=$(find . -name "*.ESM") #catch .esp file
        espFile=${espFile##*/} #Annoying little thing with the find command is it adds a ./
        finalInsertESP=$contentInsert$espFile
        echo $finalInsertESP >> $configFile
fi

count=`ls -1 *.esp 2>/dev/null | wc -l` #This installs any esp files
if [ $count != 0 ]
then
	espFile=$(find . -name "*.esp") #catch .esp file
	espFile=${espFile##*/} #Annoying little thing with the find command is it adds a ./
	finalInsertESP=$contentInsert$espFile
	echo $finalInsertESP >> $configFile
fi

count=`ls -1 *.ESP 2>/dev/null | wc -l` #This installs any ESP files
if [ $count != 0 ]
then
        espFile=$(find . -name "*.ESP") #catch .esp file
        espFile=${espFile##*/} #Annoying little thing with the find command is it adds a ./
        finalInsertESP=$contentInsert$espFile
        echo $finalInsertESP >> $configFile
fi


count=`ls -1 *.omwaddon 2>/dev/null | wc -l` #This installs any esp files
if [ $count != 0 ]
then
        espFile=$(find . -name "*.omwaddon") #catch .esp file
        espFile=${espFile##*/} #Annoying little thing with the find command is it adds a ./
        finalInsertESP=$contentInsert$espFile
        echo $finalInsertESP >> $configFile
fi

count=`ls -1 *.OMWADDON 2>/dev/null | wc -l` #This installs any esp files
if [ $count != 0 ]
then
        espFile=$(find . -name "*.OMWADDON") #catch .esp file
        espFile=${espFile##*/} #Annoying little thing with the find command is it adds a ./
        finalInsertESP=$contentInsert$espFile
        echo $finalInsertESP >> $configFile
fi

lineNumberDIR=$(grep -n "data=" $configFile | tail -1 | cut -d : -f 1) #Find last instance of data= in openmw.cfg
lineNumberDIR="$((lineNumberDIR + 1))" #Add one to lineNumber


sed -i $lineNumberDIR"i $finalInsertDIR" $configFile # simple sed command to insert  the dir into openmw.cfg



